package JDBCPack;

import java.sql.Connection;
import java.sql.DriverManager;
public class DriverDemo {
	private DriverDemo() {}
	private static Connection con;
	static {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	private static String userid="sreelekha";
	private static String password="Raghu@1999";
	private static String uri="jdbc:mysql://localhost/coda";
	private static final ThreadLocal<Connection> tLocal=new ThreadLocal<Connection>();
	synchronized public static Connection getConnection() {
			con=tLocal.get();
			if(con==null) {
				try {
					con=DriverManager.getConnection(uri,userid,password);
					tLocal.set(con);
					return con;
				}catch(Exception e) {
					e.printStackTrace();
					return null;
				}
			}
			return con;
	}
	synchronized public static void removeConnection() {
		con=tLocal.get();
		if(con!=null) {
			try {
			con.close();
			tLocal.remove();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
}